

English:

 

This Plugin for all of you who want to change Join and Quit message of your server.

You can set Custom messages for "OP" Players and "Normal" Players.

 

Permissions:

customjoin.join.op  --  Allows a Player to use the OP join message (OP: standard)

customjoin.quit.op  --  Allows a Player to use the OP quit message (OP: standard)

 

Standard look:

[+] Playername  --  (OP: red , NORMAL: green)

[-] Playername --  (OP: red , NORMAL: green)

 

Config help:

Do only edit the message it self otherwise it wont work.

To display the Playername enter '[Player]' whitout strings.

 

 

German:

Mit diesem Plugin lassen sich ganz einfach die Join und Quit Nachrichten des Servers ändern.

Es giebt die Möglichkeit verschiedene Nachrichten für Opratoren und normale Spieler fest zu legen.

 

Permissions:

customjoin.join.op  --  Erlaubt einem Spieler die join Nachricht für das Team zu nutzen (OP: standard)

customjoin.quit.op  --  Erlaubt einem Spieler die quit Nachricht für das Team zu nutzen (OP: standard)

 

Standart Aussehen:

[+] Spielername  --  (OP: rot, NORMAL: grün)

[-] Spielername  --  (OP: rot, NORMAL: grün)

 

Konfigurationshilfe:

Ändere nur die Nachricht an sich. Ansonsten wird das Plugin nicht arbeiten können. 

Um einen Spielernamen anzeigen zu lassen, verwende '[Player]' ohne Strings.
